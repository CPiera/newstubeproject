package hello.application;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import hello.application.dto.UserDTO;
import hello.domain.User;
import hello.persistance.UserRepository;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;


@Controller
public class UserController {
	@Autowired
	private UserRepository userRepository;

	public UserDTO register(UserDTO userDTO)
			throws InvalidParamException, NotFoundException {
		userRepository.checkNotRepeatedName(userDTO.getName());
		userRepository.checkNotRepeatedEmail(userDTO.getEmail());

		User user = new User(userDTO.getName(), userDTO.getEmail(), userDTO.getPassword());
		userRepository.save(user);
		return new UserDTO(user);

	}

	public UserDTO loginUser(UserDTO userToLogin) throws InvalidParamException, NotFoundException {
		User user = userRepository.getUserByEmail(userToLogin.getEmail());
		user.checkPasswordCorrect(userToLogin.getPassword());
		return new UserDTO(user);
	}

	public List<UserDTO> getAllUsers() throws NotFoundException {
		List<UserDTO> usersDTO = new ArrayList<>();
		List<User> users = userRepository.getAllUsers();
		for (User u : users) {
			usersDTO.add(new UserDTO(u));
		}
		return usersDTO;
	}

	User getUser(int userId) throws NotFoundException { 
		return userRepository.getUserById(userId);
		
	}

	public UserDTO getUserDTO(int userId) throws NotFoundException {
		User userDTO = getUser(userId);
		return new UserDTO(userDTO); 
	}

	public UserDTO updateUser(int userId, UserDTO userToUpdate) throws NotFoundException, InvalidParamException {
		userRepository.checkUserExist(userId);
		User user = userRepository.getUserById(userId);	
		user.setName(userToUpdate.getName());
		user.setEmail(userToUpdate.getEmail());
		userRepository.save(user);
		return new UserDTO(user);
	}




}
