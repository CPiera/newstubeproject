package hello.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import hello.application.dto.VideoDTO;
import hello.domain.User;
import hello.domain.Video;
import hello.persistance.VideoRepository;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Controller

public class VideoController {
	@Autowired
	private VideoRepository videoRepository;
	@Autowired
	private UserController userController;

	public VideoDTO uploadVideo(int userId, VideoDTO videoToUpload) throws InvalidParamException, NotFoundException {
		User user = userController.getUser(userId);

		Video video = new Video(user, videoToUpload.getTitle(), videoToUpload.getVideoUrl(), videoToUpload.getDescription(), videoToUpload.getImageUrl());

		videoRepository.save(video);

		return new VideoDTO(video);

	}

	public List<VideoDTO> getAllVideos(int userId) throws NotFoundException {
		User user = userController.getUser(userId);
		List<Video> result = videoRepository.getAllVideos(user);
		List<VideoDTO> resultDTO = new ArrayList<>();
		for (Video v: result) {
			resultDTO.add(new VideoDTO(v));
		}
		return resultDTO;
	}

	public VideoDTO getVideoFromUser(int userId, int videoId) throws NotFoundException, InvalidParamException {
		Video video = videoRepository.getVideoById(videoId);
		if (userId != video.getUserId()) {
			throw new InvalidParamException();
		}
		return new VideoDTO(video);
		
	}

	public void removeAllVideos(int userId) throws NotFoundException {
		User user = userController.getUser(userId);
		videoRepository.removeAllVideos(user);
			
	}

	public void removeVideoFromUser(int userId, int videoId) throws NotFoundException, InvalidParamException {
		User user = userController.getUser(userId);
		Video video = videoRepository.getVideoById(videoId);
		if (userId != video.getUserId()) {
			throw new InvalidParamException();
		}
		videoRepository.removeVideo(user, videoId);
	}
		
}

	
	
	
	
	
