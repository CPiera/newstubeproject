package hello.application.dto;

import hello.domain.User;
import hello.utilities.NotFoundException;

public class UserDTO {

	private int id;
	private String name;
	private String email;
	private String password;

	public UserDTO(User user) throws NotFoundException {
		if (user == null)
			throw new NotFoundException();
		this.name = user.getName();
		this.id = user.getIdUser();
		this.email = user.getEmail();
		this.password = user.getPassword();
	}

	public int getIdUser() {
		return id;
	}

	public String getName() {
		if(this.name==null) return "";
		return this.name;
	}

	public String getEmail() {
		if(this.email==null) return "";
		return this.email;
	}

	public String getPassword() {
		if(this.password==null) return "";
		return this.password;
	}
	
}
