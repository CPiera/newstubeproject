package hello.application.dto;

import hello.domain.Video;
import hello.utilities.NotFoundException;

public class VideoDTO {

	private Integer id;
	private String title, description, videoUrl, imageUrl;

	public VideoDTO(Video video) throws NotFoundException {
		if (video == null)
			throw new NotFoundException();
		this.id = video.getId();
		this.title = video.getTitle();
		this.description = video.getDescription();
		this.videoUrl = video.getVideoUrl();
		this.imageUrl = video.getImageUrl();

	}

	public Integer getIdVideo() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getVideoUrl() {
		return videoUrl;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

}
