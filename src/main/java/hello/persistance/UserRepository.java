package hello.persistance;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hello.domain.User;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Repository
public class UserRepository {

	@Autowired
	private HelperUserRepository repository;

	public void save(User user) throws InvalidParamException {
		if (user == null)
			throw new InvalidParamException();
		try {
			repository.save(user);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidParamException("no s'ha guardat ususari: usuari repetit o ...");
		}

	}

	public User getUserByEmail(String email) throws NotFoundException, InvalidParamException {
		User user = repository.findByEmail(email);
		if (user == null)
			throw new InvalidParamException();
		return user;

	}
	
	public List<User> getAllUsers(){
		List<User> result=new ArrayList<>();
		for (User u: repository.findAll()) {
			result.add(u);
		}
		return result;
	}

	public User getUserById(int userId) throws NotFoundException {
		try {
			return repository.findById(userId).get();
		}catch(Exception e) {
			throw new NotFoundException();
		}
		
		
	} 
	
	public void checkNotRepeatedName(String name) throws InvalidParamException {
		User user = repository.findByName(name);
		if (user != null) 
			throw new InvalidParamException("Nom repetit");
	}

	public void checkNotRepeatedEmail(String email) throws InvalidParamException {
		User user =  repository.findByEmail(email);
		if (user != null) 
				throw new InvalidParamException("Email repetit");
		

	}

	
	public void checkUserExist(int identifier) throws NotFoundException {
		List<User> users = getAllUsers();
		for (User u : users) {
			if (u.getIdUser() == identifier)
				return;
		}
		throw new NotFoundException("Jugador inexistent");

	}
	
	

}
