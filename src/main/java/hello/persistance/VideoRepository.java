package hello.persistance;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import hello.application.dto.VideoDTO;
import hello.domain.User;
import hello.domain.Video;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@Repository
public class VideoRepository {

	@Autowired
	private HelperVideoRepository repository;

	public void save(Video video) throws InvalidParamException {
		if (video == null)
			throw new InvalidParamException();
		try {
			repository.save(video);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidParamException("no s'ha guardat el video");
		}
	}

	public List<Video> getAllVideos(User user) {
		List<Video> result = repository.findAllByUser(user);
		return result;
	}

	public Video getVideoById(int videoId) throws NotFoundException {
		try {
			return repository.findById(videoId).get();
		} catch (Exception e) {
			throw new NotFoundException();
		}
	}

	public void removeAllVideos(User user) {
		repository.removeByUser(user);
		
	}

	public void removeVideo(User user, int videoId) throws NotFoundException {
		List<Video> result = repository.findAllByUser(user);
		for (Video v: result) {
			if(v.getId()==videoId) {
				repository.deleteById(videoId);
			}throw new NotFoundException();
		}
		
		
	}
	


}
