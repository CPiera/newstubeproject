package hello.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import hello.utilities.InvalidParamException;

@Entity(name = "video")
public class Video {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	private String title, description;
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name ="user_id")
	private User user;
	private String video_url;
	private String image_url;
	
	public Video() {

	}

	public Video(User user, String title, String video_url , String description, String image_url) throws InvalidParamException {
		if (user == null)
			throw new InvalidParamException();
		
		if (title.equals("") || description.equals(""))
			throw new InvalidParamException();

		if (!video_url.contains(".com"))
			throw new InvalidParamException();

		this.user = user;
		this.title = title;
		this.description = description;
		this.video_url = video_url;
		this.image_url = image_url;

	}
	

	public User getUser() {
		return user;
	}

	public Integer getUserId() {
		return user.getIdUser();
	}
	
	
	
	public Integer getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getVideoUrl() {
		return video_url;
	}

	public String getImageUrl() {
		return image_url;
	}

}
