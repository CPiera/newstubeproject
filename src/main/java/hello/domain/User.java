package hello.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import hello.utilities.Encryptor;
import hello.utilities.InvalidParamException;

@Entity(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	private String name;
	private String email;

	private String password;

	public User() {

	}

	public User(String name, String email, String password) throws InvalidParamException {
		checkName(name);
		checkEmail(email);
		checkPassword(password);
	
		this.email = email;
		this.password = Encryptor.encryptPassword(password);
		this.name = name;
	}

	public int getIdUser() {
		return id;
	}
	

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void checkPasswordCorrect(String password) throws InvalidParamException {
		Encryptor.checkIfPasswordMatches(password, this.password);
	}
	
	public void checkPassword(String password) throws InvalidParamException {
		if (password.length() < 7 || password.equals(""))
			throw new InvalidParamException();

	}
	
	public void checkName(String name) throws InvalidParamException {
		if (name.equals(""))
			throw new InvalidParamException();

	}
	
	public void checkEmail(String email) throws InvalidParamException {
		if (!email.contains("@")|| email.equals(""))
			throw new InvalidParamException();

	}

	public void setName(String name) throws InvalidParamException {
		if (!name.equals(""))
			this.name = name;

	}

	public void setEmail(String email) throws InvalidParamException {
		if (!email.equals("")) {
			checkEmail(email);
			this.email = email;
		}
			
	}

}
