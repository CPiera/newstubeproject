package hello.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import hello.application.UserController;
import hello.application.dto.UserDTO;
@CrossOrigin
@RestController
public class UserRestController {

	@Autowired
	private UserController controller;

	@PostMapping(value = "/users", produces = "application/json;charset=UTF-8")
	public String registerUser(@RequestBody String jUser) throws Exception {
		UserDTO userToCreate = new Gson().fromJson(jUser, UserDTO.class);
		UserDTO user = controller.register(userToCreate);
		return new Gson().toJson(user);

	}

	@PostMapping("/login")
	public String loginUser(@RequestBody String jUser) throws Exception {
		UserDTO userToLogin = new Gson().fromJson(jUser, UserDTO.class);
		UserDTO user = controller.loginUser(userToLogin);
		return new Gson().toJson(user);
	}

	@GetMapping(value = "/users/{userId}", produces = "application/json;charset=UTF-8")
	public String getUser(@PathVariable int userId) throws Exception {
		UserDTO user = controller.getUserDTO(userId);
		return new Gson().toJson(user);
	}

	@PutMapping(value = "/users/{userId}", produces = "application/json;charset=UTF-8")
	public String updatePlayerName(@PathVariable int userId, @RequestBody String jUser) throws Exception {

		UserDTO userToUpdate = new Gson().fromJson(jUser, UserDTO.class);

		UserDTO user = controller.updateUser(userId, userToUpdate);

		return new Gson().toJson(user);
	}
	


}
