package hello.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import hello.application.VideoController;
import hello.application.dto.VideoDTO;
import hello.utilities.InvalidParamException;
import hello.utilities.NotFoundException;

@CrossOrigin
@RestController
@RequestMapping("/users/{userId}/videos")
public class VideoRestController {

	@Autowired
	private VideoController controller;

	@PostMapping
	public String upload(@PathVariable int userId, @RequestBody String jVideo)
			throws InvalidParamException, NotFoundException {
		VideoDTO video = new Gson().fromJson(jVideo, VideoDTO.class);
		VideoDTO result = controller.uploadVideo(userId, video);
		return new Gson().toJson(result);
	}

	
	
	@GetMapping
	public String getUserVideos(@PathVariable int userId) throws Exception {
		List<VideoDTO> videos = controller.getAllVideos(userId);
		return new Gson().toJson(videos);
	}
	
	@GetMapping (value = "/{videoId}", produces = "application/json;charset=UTF-8")
	public String getUserVideo(@PathVariable int userId, @PathVariable int videoId) throws Exception {
		VideoDTO video = controller.getVideoFromUser(userId,videoId);
		return new Gson().toJson(video);
	}
	
	@DeleteMapping
	public void removeAllVideos(@PathVariable int userId) throws Exception {
		controller.removeAllVideos(userId);
	}
	
	@DeleteMapping (value = "/{videoId}", produces = "application/json;charset=UTF-8")
	public void removeVideoFromUser(@PathVariable int userId, @PathVariable int videoId) throws Exception {
		controller.removeVideoFromUser(userId,videoId);
	}
	
	
	
	
	
	

}
